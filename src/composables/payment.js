import { ref } from 'vue'
import axios from 'axios'

export default function usePayments() {
  const payments = ref([])
  const errors = ref([])

  const getPayments = async () => {
    const response = await axios.get('http://localhost:8000/api/payment')

    payments.value = response.data
  }

  const storePayment = async (data) => {
    try {
      const response = await axios.post('http://localhost:8000/api/payment', data)
      await getPayments()
      return response
    } catch (error) {
      if (error.response.status === 422) {
        return error.response
      }
    }
  }

  return {
    payments,
    errors,
    getPayments,
    storePayment
  }
}
